#===============================================================================
#         FILE: Music.pm
#        FILES: ---
#       AUTHOR: (Steven Sun)
#      VERSION: 1.0
#      CREATED: 10/28/2014 17:59:56
#===============================================================================

package CD::Music;
use strict;

{
    my $_count = 0;
    sub get_count{$_count}
    sub _incr_count{++$_count}
}

sub new {
    my ($class, @arg) = @_;
    $class->_incr_count();
    bless{
        name => $_[1],
        singer => $_[2],
        album => $_[3],
        rating => $_[4],
    }, $class;
}

sub name{ $_[0] -> {name} }
sub singer{ $_[0] -> {singer} }
sub album{ $_[0] -> {album} }

sub rating {
    my ($self, $rating) = @_;
    $self -> {rating} = $rating if $rating;
}

#package main;

##create 100 CD::Music objects,
#for(1..100){
    #CD::Music->_incr_count();
#}
#print "There have been ", CD::Music->get_count(), " CDs created\n";

package main;
#create an object storing a CD's details
my $cd = CD::Music->new("谁的眼泪在飞", "孟庭苇", "谁的眼泪在飞", 7);

#get the CD's name;
print $cd->name, "\n";

#get the singer's name
print $cd->singer."\n";

#get the album's name
print $cd->album, "\n";

#get the rating of the song
#$cd->rating();
print $cd->rating()."\n";

##print out how many songs
print "There have been ", CD::Music->get_count(), " CD[s] created\n";
